<?php

/* Global con la url de la prueba de la cual hago scraping de los datos */
define("URL_SCRAP", "https://sandbox5.dobcn.com/hiring/david/");

/**
 * Funcion para recuperar todos los productos del catalogo
 * return JSON
 */
function getAllProducts(){
	/* Uso un require_once para usar la libreria que me permite hacer un scraping simple del dom */
	require_once 'simple_html_dom.php';
	//definicion de un array vacio que almacenara todos los datos para transformarlo luego en JSON
	$arrProduct = array();
	
	//segun el idioma de la pagina recuperara los datos de esa manera
	if(isset($_SESSION["lang"]) ){
		$language = $_SESSION["lang"];
		
	}else{
		$language = "es";
		
	}

    switch ($language) {
    	case 'en':
    		$extraUrl = 'en/catalog/';
    		break;
		case 'pt':
    		$extraUrl = 'pt/catalogo/';
    		break;
    	
    	default:
    		$extraUrl = 'catalogo/';
    		break;
	}
	//comienzo del scraping
	$html = file_get_html( URL_SCRAP . $extraUrl);

	$catalogo = $html->find('#catalog-container', 0);
	
	$productos = $catalogo->find('.catalog-product');
	
	//recorro todos los productos del DOM de la pagina
	foreach ($productos as $producto) {
		$urlImg = $producto->find('.prod-img', 0)->src;
		$title = trim($producto->find('.prod-title', 0)->plaintext);
		$subtitle = $producto->find('.prod-subtitle', 0)->plaintext;
		$flavour = $producto->find('.prod-flavour', 0)->plaintext;
		$cn = $producto->find('.prod-cn', 0)->plaintext;

		$link = $producto->find('.purchase-button', 0)->href;
		$linkText = $producto->find('.purchase-button', 0)->plaintext;

		$links = explode("/", $link);

		$category = explode(" ", $title);
		
		
		//los añado al array
		array_push($arrProduct, array(
			'urlImg' => $urlImg, 
			'title' => $title,
			'subtitle' => $subtitle,
			'flavour' => $flavour,
			'cn' => $cn,
			'category' => $category[0],
			'link' => $links[count($links)-2],
			'linkText' => $linkText
		) );
		
	}
	
	//retorno los datos codificandolos como JSON
	return json_encode($arrProduct);
}
/**
 * Funcion que recupera los inputs y labels de los filtros
 * return JSON
 */
function getFilters(){
	/* Uso un require_once para usar la libreria que me permite hacer un scraping simple del dom */
	require_once 'simple_html_dom.php';

	//segun el idioma de la pagina recuperara los datos de esa manera
	if(isset($_SESSION["lang"]) ){
		$language = $_SESSION["lang"];
		
	}else{
		$language = "es";
		
	}

    switch ($language) {
    	case 'en':
    		$extraUrl = 'en/catalog/';
    		break;
		case 'pt':
    		$extraUrl = 'pt/catalogo/';
    		break;
    	
    	default:
    		$extraUrl = 'catalogo/';
    		break;
	}
	//comienzo del scraping
	$html = file_get_html( URL_SCRAP . $extraUrl);
	//filter-by-family-box > button-collapse
	//clear-all-filters

	$textCollapse = $html->find(".filter-by-family-box > .button-collapse > p", 0)->plaintext;
	$textClearFilter = $html->find(".clear-all-filters", 0)->plaintext;

	$filtrosGroup = $html->find('#collapseFamily', 0);

	$arrayFilters = array();
	$filtros = $filtrosGroup->find('.family');

	foreach ($filtros as $key => $filtro) {
		$label = $filtro->find("label", 0)->plaintext;
		$inputId = $filtro->find("input", 0)->id;
		$inputType = $filtro->find("input", 0)->type;
		$inputValue = $filtro->find("input", 0)->value;
		//los añado al array
		$arrayFilters["filtros"][$key] = array(
			'label' => $label, 
			'inputId' => $inputId,
			'inputType' => $inputType,
			'inputValue' => $inputValue
		);
	}
	$arrayFilters["textCollapse"] = $textCollapse;
	$arrayFilters["textClearFilter"] = $textClearFilter;
	
	//retorno los datos codificandolos como JSON
	return json_encode($arrayFilters);
}
/**
 * Funcion que devuelve la descripcion de un solo producto y recibe un string como parametro
 * $product String
 * return JSON
 */
function getSingleProduct($product){
	/* Uso un require_once para usar la libreria que me permite hacer un scraping simple del dom */
	require_once 'simple_html_dom.php';
	//segun el idioma de la pagina recuperara los datos de esa manera
	if(isset($_SESSION["lang"]) ){
		$language = $_SESSION["lang"];
		
	}else{
		$language = "es";
		
	}

    switch ($language) {
    	case 'en':
    		$extraUrl = 'en/producto/';
    		break;
		case 'pt':
    		$extraUrl = 'pt/producto/';
    		break;
    	
    	default:
    		$extraUrl = 'catalogo/';
    		break;
    }
	//comienzo del scraping
	$html = file_get_html( URL_SCRAP . $extraUrl . $product );
	$datosProducto = array();

	$fichaProducto = $html->find(".product-preview", 0);

	$imgProd = $fichaProducto->find( ".prod-img", 0)->src;
	$title = trim($fichaProducto->find('.prod-title', 0)->plaintext);
	$subtitle = $fichaProducto->find('.prod-subtitle', 0)->plaintext;
	$flavour = $fichaProducto->find('.prod-flavour', 0)->plaintext;
	$cn = $fichaProducto->find('.prod-cn', 0)->plaintext;

	$datosProducto["product"] = array(
		'urlImg' => $imgProd, 
		'title' => $title,
		'subtitle' => $subtitle,
		'flavour' => $flavour,
		'cn' => $cn,
	);

	$aboutProducto = $html->find(".about-the-prod", 0);

	$desc = $aboutProducto->find("div > p", 0)->plaintext;

	$extraData = $aboutProducto->find("table td");
	//definicion de dos contadores externos para almacenar de forma pareja el icono y el texto
	$contadorImg = 0;
	$contadortext = 0;
	foreach ($extraData as $key => $value) {
		

		$iconImg = $value->find("img");
	
		foreach ($iconImg as $key1 => $value1) {
			
			$icon = $value1->src;
			
			$datosProducto["aboutProduct"]["extradata"][$contadorImg]["icon"] = $icon;
			$contadorImg++;
		}

		$textIcon = $value->find("p");
		
		foreach ($textIcon as $key2 => $value2) {
			$text = $value2->plaintext;
			
			$datosProducto["aboutProduct"]["extradata"][$contadortext]["text"] = $text;
			$contadortext++;
		}
		

		
	}

	$datosProducto["aboutProduct"]["desc"] = $desc;
	
	//codifico el array como como JSON
	$datosProducto = json_encode($datosProducto);
	//retorno los datos 
	return $datosProducto;
}

?>