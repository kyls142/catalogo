<?php session_start(); ?>
<!doctype html>
<?php

$idiomas = ["es", "en", "pt"];

if(isset($_POST["lang"]) && in_array( $_POST["lang"], $idiomas ) ){
	$language = $_POST["lang"];
	$_SESSION["lang"] = $language;
}
if(isset($_SESSION["lang"]) && in_array( $_SESSION["lang"], $idiomas ) ){
	$language = $_SESSION["lang"];
	
}else{
	$language = "es";
	
}
include("../../controller/functions.php");

$ficha = json_decode(getSingleProduct($_GET["producto"]));


?>
<html lang="<?php echo $language ?>">
	<head>
		<?php include("../../commons/head.php"); ?>
		<title><?php echo $ficha->product->title; ?> - Prueba</title>
		<meta name="description" content="<?php echo $ficha->aboutProduct->desc; ?>">
		<link rel="stylesheet" href="../../assets/css/main.css">
	</head>
	<body>
		<?php include("../../commons/navbar.php"); ?>
		<main>
			<article id="catalog">
				<div class="container">
					<div class="row mt-5">
						
						<?php 
							$producto = "";	
							
							
								
								$producto .= '<div class="col-12 col-lg-3">';
								$producto .= '<img class="img-fluid" src="' . $ficha->product->urlImg . '" alt="">';
								$producto .= '</div>';
								$producto .= '<div class="col-12 col-lg-4">';
								$producto .= '<h2>' . $ficha->product->title . '</h2>';
								$producto .= '<h4>' . $ficha->product->subtitle . '</h4>';
								$producto .= '<p>' . $ficha->product->flavour . '</p>';
								$producto .= '<p>' . $ficha->product->cn . '</p>';
								$producto .= '</div>';
								$producto .= '<div class="col-12 col-lg-5">';
								$producto .= '<p>' . $ficha->aboutProduct->desc . '</p>';
								$producto .= '<table>';
								$producto .= '<tbody>';
								foreach ($ficha->aboutProduct->extradata as $key => $value) {
									$producto .= '<tr>';
									$producto .= '<td>';
									$producto .= '<img src="' . $value->icon . '" alt="">';
									$producto .= '</td>';
									$producto .= '<td>';
									$producto .= '<p>' . $value->text . '</p>';
									$producto .= '</td>';
									$producto .= '</tr>';
								}
								$producto .= '</tbody>';
								$producto .= '</table>';
								$producto .= '</div>';
							
						
								echo $producto;
						?>
						<script>

						</script>
					</div>
				</div>
			</article>
		</main>
		<footer>
			<?php include("../../commons/footer.php"); ?>
			<script src="../../assets/js/main.js"></script>
		</footer>
	</body>
</html>