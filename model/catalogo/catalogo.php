<!doctype html>
<?php

$idiomas = ["es", "en", "pt"];

if(isset($_POST["lang"]) && in_array( $_POST["lang"], $idiomas ) ){
	$language = $_POST["lang"];
	$_SESSION["lang"] = $language;
}
if(isset($_SESSION["lang"]) && in_array( $_SESSION["lang"], $idiomas ) ){
	$language = $_SESSION["lang"];
	
}else{
	$language = "es";
	$_SESSION["lang"] = $language;
	
}
?>
<html lang="<?php echo $language ?>">
	<head>
		<?php include("commons/head.php"); ?>
		<?php include("controller/functions.php"); ?>
		<title>Catalogo de productos - Prueba</title>
		<meta name="description" content="Catalogo Prueba de David Quintana Fouret">
		<link rel="stylesheet" href="assets/css/main.css">
	</head>
	<body>
	
		<?php include("commons/navbar.php"); ?>
		<main>
			<article id="catalog">
				<div class="container">
					<div class="row">
						<div class="col-md-4 mt-5">
							<div id="filtros">
							<?php 
								$jsonFilters = json_decode(getFilters());
								$filtros ='<button class="button-collapse btn mb-3" type="button" data-toggle="collapse" data-target="#collapseFamily" aria-expanded="true" aria-controls="collapseExample">';
								$filtros .= $jsonFilters->textCollapse;
								$filtros .= "</button>";
								$filtros .= "<div id='collapseFamily' class='collapsable collapse show'>";
								foreach ($jsonFilters->filtros as $key => $value) {
									$filtros .= "<div class='family'>";
									$filtros .= "<input id='" . $value->inputId . "' type='" . $value->inputType . "' value='" . $value->inputValue . "'>";
									$filtros .= "<label for='" . $value->inputId . "'>" . $value->label . "</label>";
									$filtros .= "</div>";
								}
								$filtros .= "</div>";
								$filtros .= "<button class='btn btn-outline-danger d-none deleteFilter'>" . $jsonFilters->textClearFilter . "</button>";
								echo $filtros;
							?>
							</div>
						</div>
						<div class="col-md-8">
							<div id="catalogo" class="row">
								
								<?php 
									$jsonProd = getAllProducts();
								?>

								<?php 
									$productos = json_decode($jsonProd);
									$item = "";
									foreach ($productos as $key => $value) {
										$item .= "<div class='mb-5 col-xs-12 col-md-6 col-lg-4 " . strtolower( $value->category ) . "'>";
										$item .= "<img class='img-fluid' src='" . $value->urlImg . "'>";
										$item .= "<h3>" . $value->title . "</h3>";
										$item .= "<h4>" . $value->subtitle . "</h4>";
										$item .= "<p>" . $value->flavour . "</p>";
										$item .= "<p>" . $value->cn . "</p>";
										$item .= "<a class='btn btn-success' href='model/producto/producto.php?producto=" . $value->link . "'>" . $value->linkText . "</a>";
										$item .= "</div>";
									}
									echo $item;
								?>
								
							</div>
						</div>
					</div>
				</div>
			</article>
		</main>
		<footer>
			<?php include("commons/footer.php"); ?>
			<script src="assets/js/main.js"></script>
		</footer>
	</body>
</html>