$(document).ready(function() {

    //evento click sobre los checkbox de los filtros
    $("#filtros input").on("click", function() {
        let arrayFilter = [];
        $("#filtros input").each(function() {
            if ($(this).is(':checked')) {
                let idFilter = $(this).attr("id");
                arrayFilter.push(idFilter);
            }
        });
        if (arrayFilter.length > 0) {
            toggleProducts(arrayFilter);
        } else {
            $("#catalogo > div").removeClass("d-none");
            $(".deleteFilter").addClass("d-none");
        }
    });
    //evento click para el boton borrarfiltros
    $(".deleteFilter").on('click', function() {
        $("#filtros input[type=checkbox]").prop("checked", false);
        $("#catalogo > div").removeClass("d-none");
        $(".deleteFilter").addClass("d-none");
    });
    //funcion que mostrara o no los productos segun su categoria
    function toggleProducts(arrayFilter) {
        $("#catalogo > div").addClass("d-none");
        $(".deleteFilter").removeClass("d-none");

        for (var i = 0; i < arrayFilter.length; i++) {
            $("." + arrayFilter[i]).removeClass("d-none");
        };
    }
    //multilang ejecucion al cambiar de idioma
    $("#language select").on("change", function() {
        $("#language").submit();
    });
});