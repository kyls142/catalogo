<nav class="navbar navbar-expand-lg navbar-dark bg-dark" >
	<div class="ml-auto">
		<ul class="navbar-nav mr-auto">
        </li>
		  <li class="nav-item">
		  	<div class="dropdown nav-link" style="display:inline-block;" >
			  <form id="language" action="" method="POST">
			  	<div class="form-group">
				  	<select name="lang" id="lang" custom-select>
				  		<option <?php echo (0 == strcmp($language, "es")) ? "selected" : ""; ?> value="es" >Es</option>
				  		<option <?php echo (0 == strcmp($language, "en")) ? "selected" : ""; ?> value="en" >Eng</option>
				  		<option <?php echo (0 == strcmp($language, "pt")) ? "selected" : ""; ?> value="pt" >Pt</option>
				  	</select>
			  	</div>
			  </form>
			</div>
		  </li>
		</ul>
	</div>
</nav>